# rubocop:disable Metrics/BlockLength
RSpec.describe CV2 do
  it 'has a version number' do
    expect(CV2.__version__).not_to be nil
    expect(CV2.__version__).to match(/^(?:\d+\.?)+$/)
  end

  it 'should be able to read jpg images' do
    file = File.expand_path(File.join(__FILE__, '../data/aero1.jpg'))

    default   = CV2.imread(file)
    color     = CV2.imread(file, CV2.IMREAD_COLOR)
    grayscale = CV2.imread(file, CV2.IMREAD_GRAYSCALE)
    unchanged = CV2.imread(file, CV2.IMREAD_UNCHANGED)

    expect(default)  .not_to be nil
    expect(color)    .not_to be nil
    expect(grayscale).not_to be nil
    expect(unchanged).not_to be nil
    expect(default)  .to     eq color
  end
  it 'should be able to read png images' do
    file = File.expand_path(File.join(__FILE__, '../data/basketball1.png'))

    default   = CV2.imread(file)
    color     = CV2.imread(file, CV2.IMREAD_COLOR)
    grayscale = CV2.imread(file, CV2.IMREAD_GRAYSCALE)
    unchanged = CV2.imread(file, CV2.IMREAD_UNCHANGED)

    expect(default)  .not_to be nil
    expect(color)    .not_to be nil
    expect(grayscale).not_to be nil
    expect(unchanged).not_to be nil
    expect(default)  .to     eq color
  end

  it 'should be able to draw on an image' do
    file = File.expand_path(File.join(__FILE__, '../data/basketball1.png'))

    orig = CV2.imread(file)
    line = CV2.imread(file)
    rect = CV2.imread(file)
    circ = CV2.imread(file)

    CV2.line(
      line,
      PyCall::Tuple.new(0, 0), PyCall::Tuple.new(511, 511),
      PyCall::Tuple.new(255, 0, 0), 5
    )
    CV2.rectangle(
      rect,
      PyCall::Tuple.new(10, 10), PyCall::Tuple.new(501, 501),
      PyCall::Tuple.new(0, 255, 0), 3
    )
    CV2.circle(
      circ,
      PyCall::Tuple.new(20, 20), 63,
      PyCall::Tuple.new(0, 0, 255), -1
    )

    expect(orig.inspect).not_to eq line.inspect
    expect(line.inspect).not_to eq rect.inspect
    expect(rect.inspect).not_to eq circ.inspect
    expect(circ.inspect).not_to eq orig.inspect
  end
end
# rubocop:enable Metrics/BlockLength

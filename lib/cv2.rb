require 'ruby.py'

# Top-level module to expose Python 'cv2' API to Ruby.
CV2 = RubyPy.import('cv2')

# Enhance the module's Ruby API.
module CV2
end
